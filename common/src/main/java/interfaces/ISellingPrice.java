package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

import entities.Car;

public interface ISellingPrice extends Remote {

	 double computeSellingPrice(Car c) throws RemoteException;

}
