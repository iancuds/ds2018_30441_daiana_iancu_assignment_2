package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.JTextArea;

public class App1 {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	JTextArea txtrTax;
	JTextArea txtrSellingprice;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					App1 window = new App1();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public App1() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 500, 393);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(30, 30, 220, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblFabricationYear = new JLabel("Fabrication year:");
		lblFabricationYear.setBounds(30, 10, 220, 20);
		frame.getContentPane().add(lblFabricationYear);
		
		JLabel lblEngineSize = new JLabel("Engine size:");
		lblEngineSize.setBounds(30, 90, 220, 20);
		frame.getContentPane().add(lblEngineSize);
		
		textField_1 = new JTextField();
		textField_1.setBounds(30, 110, 220, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblPurchasingPrice = new JLabel("Purchasing price:");
		lblPurchasingPrice.setBounds(30, 170, 220, 20);
		frame.getContentPane().add(lblPurchasingPrice);
		
		textField_2 = new JTextField();
		textField_2.setBounds(30, 190, 220, 20);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		JButton btnComputeTax = new JButton("Compute Tax");
		btnComputeTax.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnComputeTax.setBounds(250, 70, 220, 20);
		frame.getContentPane().add(btnComputeTax);
		
		JButton btnComputeSellingPrice = new JButton("Compute Selling Price");
		btnComputeSellingPrice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnComputeSellingPrice.setBounds(250, 150, 220, 20);
		frame.getContentPane().add(btnComputeSellingPrice);
		
		JLabel lblTax = new JLabel("Tax");
		lblTax.setBounds(30, 243, 46, 14);
		frame.getContentPane().add(lblTax);
		
		JLabel lblSellingPrice = new JLabel("Selling Price");
		lblSellingPrice.setBounds(253, 243, 124, 14);
		frame.getContentPane().add(lblSellingPrice);
		
		 txtrTax = new JTextArea();
		txtrTax.setText("tax");
		txtrTax.setBounds(30, 268, 103, 20);
		frame.getContentPane().add(txtrTax);
		
		 txtrSellingprice = new JTextArea();
		txtrSellingprice.setText("sellingPrice");
		txtrSellingprice.setBounds(250, 268, 110, 20);
		frame.getContentPane().add(txtrSellingprice);
	}
}
