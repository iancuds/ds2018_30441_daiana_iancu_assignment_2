package gui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JButton;

public class App {

	private JFrame frame;
	private JTextField tf;
	private JTextField tf1;
	private JTextField tf2;
	private JButton btnComputeTax;
	private JButton btnComputeSellingPrice;
	private JTextArea txtrTax;
	private JTextArea txtrSellingprice;

	
	public App() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 500, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		tf = new JTextField();
		tf.setBounds(30, 30, 220, 20);
		frame.getContentPane().add(tf);
		tf.setColumns(10);
		
		JLabel lblFabricationYear = new JLabel("Fabrication year:");
		lblFabricationYear.setBounds(30, 10, 220, 20);
		frame.getContentPane().add(lblFabricationYear);
		
		JLabel lblEngineSize = new JLabel("Engine size:");
		lblEngineSize.setBounds(30, 90, 220, 20);
		frame.getContentPane().add(lblEngineSize);
		
		tf1 = new JTextField();
		tf1.setBounds(30, 110, 220, 20);
		frame.getContentPane().add(tf1);
		tf1.setColumns(10);
		
		JLabel lblPurchasingPrice = new JLabel("Purchasing price:");
		lblPurchasingPrice.setBounds(30, 170, 220, 20);
		frame.getContentPane().add(lblPurchasingPrice);
		
		tf2 = new JTextField();
		tf2.setBounds(30, 190, 220, 20);
		frame.getContentPane().add(tf2);
		tf2.setColumns(10);
		
		 btnComputeTax = new JButton("Compute Tax");
		
		btnComputeTax.setBounds(250, 70, 220, 20);
		frame.getContentPane().add(btnComputeTax);
		
		 btnComputeSellingPrice = new JButton("Compute Selling Price");
		
		btnComputeSellingPrice.setBounds(250, 150, 220, 20);
		frame.getContentPane().add(btnComputeSellingPrice);
		
		JLabel lblTax = new JLabel("Tax");
		lblTax.setBounds(30, 243, 46, 14);
		frame.getContentPane().add(lblTax);
		
		JLabel lblSellingPrice = new JLabel("Selling Price");
		lblSellingPrice.setBounds(253, 243, 124, 14);
		frame.getContentPane().add(lblSellingPrice);
		
		 txtrTax = new JTextArea();
		txtrTax.setText("tax");
		txtrTax.setBounds(30, 268, 103, 20);
		frame.getContentPane().add(txtrTax);
		
		 txtrSellingprice = new JTextArea();
		txtrSellingprice.setText("sellingPrice");
		txtrSellingprice.setBounds(250, 268, 110, 20);
		frame.getContentPane().add(txtrSellingprice);
	}
	
	public void addActionListenerTaxBtn(ActionListener e)
	{
		btnComputeTax.addActionListener(e);
		
	}
	
	public void addActionListenerSellingPriceBtn(ActionListener e)
	{
		btnComputeSellingPrice.addActionListener(e);
	}
	
	public int getYear()
	{
		int year =Integer.parseInt( tf.getText());
		return year;
	}
	
	public int getEngineSize()
	{
		int es =Integer.parseInt( tf1.getText());
		return es;
	}
	
	public double getPurchasingPrice()
	{
		double pp =Double.parseDouble( tf2.getText());
		return pp;
	}
	
	public void setTax(double tax)
	{
		txtrTax.setText(((Double)tax).toString());
	}
	public void setSellingPrice(double sellingP)
	{
		txtrSellingprice.setText(((Double)sellingP).toString());
	}
	
	public JFrame getFrame()
	{
		return this.frame;
	}
	
	public void setFrameVisible(boolean v)
	{
		this.frame.setVisible(v);
	}
	public void clearFields()
	{
		tf.setText("");
		tf1.setText("");
		tf2.setText("");
	}
}
