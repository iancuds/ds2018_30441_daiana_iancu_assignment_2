package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import entities.Car;
import gui.App;
import interfaces.ISellingPrice;
import interfaces.ITaxService;

public class Controller {
	
	App app;
	
	private Car car;
	
	 String name1 = "TaxService";
     String name2="SellingPrice";
     ITaxService ts;
     ISellingPrice sp;
     
     
	public Controller() throws RemoteException, NotBoundException
	{
		app = new App();
		app.setFrameVisible(true);
		car = new Car(0,0,0);
		   Registry registry = LocateRegistry.getRegistry();
            ts = (ITaxService) registry.lookup(name1);
            sp = (ISellingPrice) registry.lookup(name2);
        
       app.addActionListenerSellingPriceBtn(new ComputeSellingPriceActionListener());
       app.addActionListenerTaxBtn(new ComputeTaxActionListener());
          

	}
	
	class ComputeTaxActionListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			int year = app.getYear();
			int engineCapacity = app.getEngineSize();
			double purchasingPrice = app.getPurchasingPrice();
			
			car.setEngineCapacity(engineCapacity);
			car.setPurchasingPrice(purchasingPrice);
			car.setYear(year);
			double tax=0;
			 try {
				 tax = ts.computeTax(car);
			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	         
		app.setTax(tax);
		  System.out.println(tax);
			
		}
	
	
	}
	
	class ComputeSellingPriceActionListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			int year = app.getYear();
			int engineCapacity = app.getEngineSize();
			double purchasingPrice = app.getPurchasingPrice();
			
			car.setEngineCapacity(engineCapacity);
			car.setPurchasingPrice(purchasingPrice);
			car.setYear(year);
			double sellingprice =0.0;
			  try {
				 sellingprice = sp.computeSellingPrice(car);
			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
				
			  app.setSellingPrice(sellingprice);
			   System.out.println(sellingprice);
			
		}
	
	
	}
	

	 public static void main(String args[]) {
//	        if (System.getSecurityManager() == null) {
//	            System.setSecurityManager(new SecurityManager());
//	        }
//	        try {
//	        	
//	        	
//	        	
//	        	
//	            String name1 = "TaxService";
//	            String name2="SellingPrice";
//	            Registry registry = LocateRegistry.getRegistry();
//	            ITaxService ts = (ITaxService) registry.lookup(name1);
//	            ISellingPrice sp = (ISellingPrice) registry.lookup(name2);
//	            double tax = ts.computeTax(new Car(2016, 200, 2000));
//	            double sellingprice = sp.computeSellingPrice(new Car(2016, 200, 2000 ));
//	          
//	            System.out.println(tax);
//	            System.out.println(sellingprice);
//	        } catch (Exception e) {
//	            System.err.println("ComputePi exception:");
//	            e.printStackTrace();
//	        }
//	        
	        
		 try {
			Controller controller = new Controller();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    }    

	
}
