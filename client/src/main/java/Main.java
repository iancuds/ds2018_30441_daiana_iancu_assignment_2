import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import entities.Car;
import interfaces.ISellingPrice;
import interfaces.ITaxService;

public class Main {

	

	 public static void main(String args[]) {
//	        if (System.getSecurityManager() == null) {
//	            System.setSecurityManager(new SecurityManager());
//	        }
	        try {
	        	
	        	
	        	
	        	
	            String name1 = "TaxService";
	            String name2="SellingPrice";
	            Registry registry = LocateRegistry.getRegistry();
	            ITaxService ts = (ITaxService) registry.lookup(name1);
	            ISellingPrice sp = (ISellingPrice) registry.lookup(name2);
	            double tax = ts.computeTax(new Car(2016, 200, 2000));
	            double sellingprice = sp.computeSellingPrice(new Car(2016, 200, 2000 ));
	          
	            System.out.println(tax);
	            System.out.println(sellingprice);
	        } catch (Exception e) {
	            System.err.println("ComputePi exception:");
	            e.printStackTrace();
	        }
	        
	        
	    }    

}
