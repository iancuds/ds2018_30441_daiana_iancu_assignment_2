package services;

import entities.Car;
import interfaces.ISellingPrice;

public class SellingPrice implements ISellingPrice {

	public SellingPrice()
	{
		super();
	}
		public double computeSellingPrice(Car c) {
			double result = 0.0;
			double pp = c.getPurchasingPrice();
			double year=(double)(2018-c.getYear());
			
			if((year)<7)
			{
				result = pp-((pp/7.0) * year);
				
			}
					return result;
		}
		
		



}
