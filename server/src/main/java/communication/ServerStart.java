package communication;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import interfaces.ISellingPrice;
import interfaces.ITaxService;
import services.SellingPrice;
import services.TaxService;

public class ServerStart {

	 public static void main(String[] args) {
		 
//		 System.setProperty("java.security.policy","file:///C:/Users/IANCU/eclipse-workspace/assignment2.2/server/src/main/java/policy.policy");
//	        if (System.getSecurityManager() == null) {
//	            System.setSecurityManager(new SecurityManager());
//	        }
	        
	        try {
	        	
	            String name1 = "TaxService";
	            String name2 = "SellingPrice";
	            ITaxService ts = new TaxService();
	            ISellingPrice sp = new SellingPrice();
	            
	            ITaxService stub1 =     (ITaxService) UnicastRemoteObject.exportObject(ts, 0);
	            Registry registry = LocateRegistry.createRegistry(1099);
	            registry.rebind(name1, stub1);
	            
	            ISellingPrice stub2 =    (ISellingPrice) UnicastRemoteObject.exportObject(sp, 0);
		          
		            registry.rebind(name2, stub2);
		            
	            
	            System.out.println("Compute stuff");
	        } catch (Exception e) {
	            System.err.println("ServiceStart exception:");
	            e.printStackTrace();
	        }
	        
	 }
}
